-- phpMyAdmin SQL Dump
-- version 3.5.8.1deb1
-- http://www.phpmyadmin.net
--
-- Gazda: localhost
-- Timp de generare: 29 Oct 2013 la 17:29
-- Versiune server: 5.5.34-0ubuntu0.13.04.1
-- Versiune PHP: 5.4.9-4ubuntu2.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Bază de date: `te_dev`
--

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `album`
--

CREATE TABLE IF NOT EXISTS `album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `artist` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Salvarea datelor din tabel `album`
--

INSERT INTO `album` (`id`, `artist`, `title`) VALUES
(1, 'The  Military  Wives', 'In  My  Dreams'),
(2, 'Adele', '21'),
(3, 'Bruce  Springsteen', 'Wrecking Ball (Deluxe)'),
(4, 'Lana  Del  Rey', 'Born  To  Die'),
(5, 'Gotye', 'Making  Mirrors');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `bilete`
--

CREATE TABLE IF NOT EXISTS `bilete` (
  `IdBilet` int(20) NOT NULL AUTO_INCREMENT,
  `serie` char(1) CHARACTER SET latin1 NOT NULL,
  `data_cursa` date NOT NULL,
  `ora_cursa` time NOT NULL,
  `p_initial` varchar(20) CHARACTER SET latin1 NOT NULL,
  `p_final` varchar(20) CHARACTER SET latin1 NOT NULL,
  `loc` int(2) NOT NULL,
  `pret` float NOT NULL,
  `IdCursa` int(10) DEFAULT NULL,
  `IdClient` int(10) DEFAULT NULL,
  PRIMARY KEY (`IdBilet`),
  KEY `IdCursa` (`IdCursa`),
  KEY `IdClient` (`IdClient`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=211 ;

--
-- Salvarea datelor din tabel `bilete`
--

INSERT INTO `bilete` (`IdBilet`, `serie`, `data_cursa`, `ora_cursa`, `p_initial`, `p_final`, `loc`, `pret`, `IdCursa`, `IdClient`) VALUES
(3, 'A', '2013-08-30', '11:11:00', 'Chircani', 'Sarata-Galbena', 4, 0, 39, 10),
(4, 'A', '2013-08-30', '11:11:00', 'Chircani', 'Sarata-Galbena', 5, 0, 39, 10),
(5, 'A', '2013-08-30', '11:11:00', 'Chircani', 'Sarata-Galbena', 7, 0, 39, 11),
(6, 'A', '2013-08-30', '11:11:00', 'Chircani', 'Sarata-Galbena', 8, 0, 39, 11),
(9, 'A', '2013-08-30', '11:11:00', 'Chircani', 'Sarata-Galbena', 3, 0, 39, 10),
(10, 'A', '2013-08-30', '11:11:00', 'Chircani', 'Sarata-Galbena', 6, 0, 39, 10),
(11, 'A', '2013-08-30', '11:11:00', 'Chircani', 'Sarata-Galbena', 9, 0, 39, 10),
(12, 'A', '2013-08-30', '11:11:00', 'Chircani', 'Sarata-Galbena', 10, 0, 39, 12),
(13, 'A', '2013-08-30', '11:11:00', 'Chircani', 'Sarata-Galbena', 11, 0, 39, 12),
(14, 'A', '2013-08-31', '11:11:00', 'Antonesti', 'Cantemir', 1, 0, 2, 10),
(15, 'A', '2013-08-31', '11:11:00', 'Antonesti', 'Cantemir', 2, 0, 2, 10),
(16, 'A', '2013-09-03', '11:11:00', 'Zirnesti', 'Mereseni', 1, 0, 39, 10),
(17, 'A', '2013-09-03', '12:20:00', 'Zirnesti', 'Mereseni', 1, 0, 40, 10),
(18, 'A', '2013-09-03', '12:20:00', 'Zirnesti', 'Mereseni', 2, 0, 40, 10),
(19, 'A', '2013-09-02', '11:15:00', 'Zirnesti', 'Mereseni', 1, 39.24, 39, 10),
(20, 'A', '2013-09-02', '11:15:00', 'Zirnesti', 'Mereseni', 2, 39.24, 39, 10),
(21, 'A', '2013-09-02', '11:15:00', 'Zirnesti', 'Mereseni', 5, 39.24, 39, 10),
(22, 'A', '2013-09-02', '11:15:00', 'Zirnesti', 'Mereseni', 7, 39.24, 39, 10),
(23, 'A', '2013-09-02', '11:15:00', 'Zirnesti', 'Mereseni', 8, 39.24, 39, 10),
(24, 'A', '2013-09-02', '11:15:00', 'Zirnesti', 'Mereseni', 10, 39.24, 39, 10),
(25, 'A', '2013-09-02', '11:15:00', 'Zirnesti', 'Mereseni', 11, 39.24, 39, 10),
(26, 'A', '2013-09-04', '12:30:00', 'Chisinau', 'Leova', 1, 34.2, 7, 10),
(27, 'A', '2013-09-04', '12:30:00', 'Chisinau', 'Leova', 2, 34.2, 7, 10),
(28, 'A', '2013-09-03', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(29, 'A', '2013-09-03', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(30, 'A', '2013-09-03', '11:20:00', 'Chisinau', 'Leova', 3, 34.2, 5, 10),
(31, 'A', '2013-09-03', '11:20:00', 'Chisinau', 'Leova', 6, 34.2, 5, 10),
(32, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(33, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 3, 34.2, 5, 10),
(34, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(35, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 3, 34.2, 5, 10),
(36, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(37, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 3, 34.2, 5, 10),
(38, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(39, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 3, 34.2, 5, 10),
(40, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(41, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 3, 34.2, 5, 10),
(42, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(43, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 3, 34.2, 5, 10),
(44, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(45, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 3, 34.2, 5, 10),
(46, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(47, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 3, 34.2, 5, 10),
(48, 'A', '2013-09-04', '12:30:00', 'Chisinau', 'Leova', 7, 34.2, 7, 10),
(49, 'A', '2013-09-04', '12:30:00', 'Chisinau', 'Leova', 8, 34.2, 7, 10),
(50, 'A', '2013-09-04', '12:30:00', 'Chisinau', 'Leova', 7, 34.2, 7, 10),
(51, 'A', '2013-09-04', '12:30:00', 'Chisinau', 'Leova', 8, 34.2, 7, 10),
(52, 'A', '2013-09-04', '12:30:00', 'Chisinau', 'Leova', 7, 34.2, 7, 10),
(53, 'A', '2013-09-04', '12:30:00', 'Chisinau', 'Leova', 8, 34.2, 7, 10),
(54, 'A', '2013-09-04', '12:30:00', 'Chisinau', 'Leova', 7, 34.2, 7, 10),
(55, 'A', '2013-09-04', '12:30:00', 'Chisinau', 'Leova', 8, 34.2, 7, 10),
(56, 'A', '2013-09-04', '12:30:00', 'Chisinau', 'Leova', 7, 34.2, 7, 10),
(57, 'A', '2013-09-04', '12:30:00', 'Chisinau', 'Leova', 8, 34.2, 7, 10),
(58, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(59, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(60, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(61, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(62, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(63, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(64, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(65, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(66, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(67, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(68, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(69, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(70, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(71, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(72, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(73, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(74, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(75, 'A', '2013-09-04', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(76, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(77, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(78, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(79, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(80, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(81, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(82, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(83, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(84, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(85, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(86, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(87, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(88, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(89, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(90, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(91, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(92, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(93, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(94, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(95, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(96, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(97, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(98, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(99, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(100, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(101, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(102, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(103, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(104, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(105, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(106, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(107, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(108, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(109, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(110, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 1, 34.2, 5, 10),
(111, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 2, 34.2, 5, 10),
(112, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(113, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(114, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(115, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(116, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(117, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(118, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(119, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(120, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(121, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(122, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(123, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(124, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(125, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(126, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(127, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(128, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(129, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(130, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(131, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(132, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(133, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(134, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(135, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(136, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(137, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(138, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(139, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(140, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(141, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(142, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(143, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(144, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(145, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(146, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(147, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(148, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(149, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(150, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(151, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(152, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(153, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 5, 34.2, 5, 10),
(154, 'A', '2013-09-05', '11:20:00', 'Chisinau', 'Leova', 4, 34.2, 5, 10),
(155, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 1, 54.36, 5, 10),
(156, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 2, 54.36, 5, 10),
(157, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 1, 54.36, 5, 10),
(158, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 2, 54.36, 5, 10),
(159, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 1, 54.36, 5, 10),
(160, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 2, 54.36, 5, 10),
(161, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 3, 54.36, 5, 10),
(162, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 4, 54.36, 5, 10),
(163, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 5, 54.36, 5, 10),
(164, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 1, 54.36, 5, 10),
(165, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 2, 54.36, 5, 10),
(166, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 3, 54.36, 5, 10),
(167, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 4, 54.36, 5, 10),
(168, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 5, 54.36, 5, 10),
(169, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 6, 54.36, 5, 10),
(170, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 8, 54.36, 5, 10),
(171, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 9, 54.36, 5, 10),
(172, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 6, 54.36, 5, 10),
(173, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 8, 54.36, 5, 10),
(174, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 9, 54.36, 5, 10),
(175, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 6, 54.36, 5, 10),
(176, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 8, 54.36, 5, 10),
(177, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 9, 54.36, 5, 10),
(178, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 6, 54.36, 5, 10),
(179, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 8, 54.36, 5, 10),
(180, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 9, 54.36, 5, 10),
(181, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 6, 54.36, 5, 10),
(182, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 8, 54.36, 5, 10),
(183, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 9, 54.36, 5, 10),
(184, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 10, 54.36, 5, 10),
(185, 'A', '2013-09-06', '11:20:00', 'Chisinau', 'Zirnesti', 11, 54.36, 5, 10),
(186, 'A', '2013-09-06', '13:10:00', 'Cantemir', 'Sarateni', 1, 19.44, 41, 10),
(187, 'A', '2013-09-06', '13:10:00', 'Cantemir', 'Sarateni', 2, 19.44, 41, 10),
(188, 'A', '2013-09-06', '11:20:00', 'Cantemir', 'Sarateni', 7, 19.44, 38, 10),
(189, 'A', '2013-09-06', '11:20:00', 'Cantemir', 'Sarateni', 8, 19.44, 38, 10),
(190, 'A', '2013-09-06', '11:20:00', 'Cantemir', 'Sarateni', 1, 19.44, 38, 10),
(191, 'A', '2013-09-06', '11:20:00', 'Cantemir', 'Sarateni', 2, 19.44, 38, 10),
(192, 'A', '2013-09-06', '11:20:00', 'Cantemir', 'Sarateni', 1, 19.44, 38, 10),
(193, 'A', '2013-09-06', '11:20:00', 'Cantemir', 'Sarateni', 2, 19.44, 38, 10),
(194, 'A', '2013-09-06', '11:20:00', 'Cantemir', 'Sarateni', 4, 19.44, 38, 10),
(195, 'A', '2013-09-06', '11:20:00', 'Cantemir', 'Sarateni', 5, 19.44, 38, 10),
(196, 'A', '2013-09-06', '11:20:00', 'Cantemir', 'Sarateni', 10, 19.44, 38, 10),
(197, 'A', '2013-09-06', '11:20:00', 'Cantemir', 'Sarateni', 11, 19.44, 38, 10),
(198, 'A', '2013-09-10', '11:20:00', 'Chisinau', 'Sarata Noua', 1, 29.88, 5, 10),
(199, 'A', '2013-09-10', '11:20:00', 'Chisinau', 'Sarata Noua', 2, 29.88, 5, 10),
(200, 'A', '2013-09-10', '11:30:00', 'Chisinau', 'Cahul', 1, 59.04, 6, 10),
(201, 'A', '2013-09-10', '11:30:00', 'Chisinau', 'Cahul', 2, 59.04, 6, 10),
(202, 'A', '2013-09-11', '11:20:00', 'Chisinau', 'Cahul', 3, 59.04, 5, 10),
(203, 'A', '2013-09-25', '13:10:00', 'Chisinau', 'Sarata-Galbena', 4, 18, 8, 10),
(204, 'A', '2013-09-25', '13:10:00', 'Chisinau', 'Sarata-Galbena', 5, 18, 8, 10),
(205, 'A', '2013-10-02', '12:44:00', 'Antonesti', 'Cantemir', 4, 2.88, 4, 10),
(206, 'A', '2013-10-02', '12:44:00', 'Antonesti', 'Cantemir', 5, 2.88, 4, 10),
(207, 'A', '2013-10-03', '13:20:00', 'Sarata-Galbena', 'Cahul', 4, 41.04, 7, 10),
(208, 'A', '2013-10-03', '13:20:00', 'Sarata-Galbena', 'Cahul', 5, 41.04, 7, 10),
(209, 'A', '2013-10-17', '13:10:00', 'Chisinau', 'Sarata-Galbena', 4, 18, 8, 13),
(210, 'A', '2013-10-17', '13:10:00', 'Chisinau', 'Sarata-Galbena', 5, 18, 8, 13);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Salvarea datelor din tabel `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('97dc8414a29e4bd27a4af544be803525', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:20.0) Gecko/20100101 Firefox/20.0 FirePHP/0.7.2', 1366278917, 'a:3:{s:9:"user_data";s:0:"";s:8:"username";s:8:"ichiriac";s:12:"is_logged_in";s:2:"da";}');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `clienti`
--

CREATE TABLE IF NOT EXISTS `clienti` (
  `IdClient` int(10) NOT NULL AUTO_INCREMENT,
  `nume` varchar(20) CHARACTER SET latin1 NOT NULL,
  `prenume` varchar(30) CHARACTER SET latin1 NOT NULL,
  `email` varchar(30) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`IdClient`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Salvarea datelor din tabel `clienti`
--

INSERT INTO `clienti` (`IdClient`, `nume`, `prenume`, `email`) VALUES
(10, 'testzf', 'rrr', 'igorchiriac1991@gmail.com'),
(11, 'igo', 'wwwww', 'iorchiriac1991@gmail.com'),
(12, 'testzf', 'reeee', 'yugioh852@gmail.com'),
(13, 'adasda', 'dadada', 'igor.chiriac@yopeso.com');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `curse`
--

CREATE TABLE IF NOT EXISTS `curse` (
  `IdCursa` int(10) NOT NULL AUTO_INCREMENT,
  `Ora_Prn` time NOT NULL,
  `Nume_loc` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Nr_tr` int(3) NOT NULL,
  PRIMARY KEY (`IdCursa`),
  KEY `Nume_loc` (`Nume_loc`),
  KEY `Nr_tr` (`Nr_tr`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

--
-- Salvarea datelor din tabel `curse`
--

INSERT INTO `curse` (`IdCursa`, `Ora_Prn`, `Nume_loc`, `Nr_tr`) VALUES
(1, '09:10:00', 'Chisinau', 106),
(2, '09:40:00', 'Chisinau', 107),
(3, '10:10:00', 'Chisinau', 108),
(4, '10:50:00', 'Chisinau', 109),
(5, '11:15:00', 'Chisinau', 110),
(6, '11:25:00', 'Chisinau', 111),
(7, '12:25:00', 'Chisinau', 112),
(8, '13:05:00', 'Chisinau', 113),
(9, '13:45:00', 'Chisinau', 114),
(10, '14:50:00', 'Chisinau', 115),
(11, '15:30:00', 'Chisinau', 116),
(12, '16:00:00', 'Chisinau', 117),
(13, '16:50:00', 'Chisinau', 118),
(14, '17:00:00', 'Chisinau', 119),
(15, '17:30:00', 'Chisinau', 120),
(16, '18:50:00', 'Chisinau', 121),
(17, '18:10:00', 'Chisinau', 122),
(18, '18:40:00', 'Chisinau', 123),
(19, '19:10:00', 'Chisinau', 124),
(20, '20:10:00', 'Chisinau', 125),
(21, '20:30:00', 'Chisinau', 126),
(22, '06:45:00', 'Chisinau', 101),
(23, '07:00:00', 'Chisinau', 102),
(24, '07:30:00', 'Chisinau', 103),
(25, '08:25:00', 'Chisinau', 104),
(26, '08:40:00', 'Chisinau', 105),
(27, '04:00:00', 'Cahul', 106),
(28, '04:30:00', 'Cahul', 107),
(29, '05:00:00', 'Cahul', 108),
(30, '05:20:00', 'Cahul', 109),
(31, '06:25:00', 'Cahul', 110),
(32, '07:00:00', 'Cahul', 111),
(33, '07:10:00', 'Cahul', 112),
(34, '07:30:00', 'Cahul', 113),
(35, '08:00:00', 'Cahul', 114),
(36, '09:05:00', 'Cahul', 115),
(37, '09:35:00', 'Cahul', 116),
(38, '10:25:00', 'Cahul', 117),
(39, '10:55:00', 'Cahul', 118),
(40, '12:00:00', 'Cahul', 119),
(41, '12:15:00', 'Cahul', 120),
(42, '12:40:00', 'Cahul', 121),
(43, '13:05:00', 'Cahul', 122),
(44, '13:20:00', 'Cahul', 123),
(45, '14:00:00', 'Cahul', 124),
(46, '14:20:00', 'Cahul', 125),
(47, '15:10:00', 'Cahul', 126),
(48, '15:20:00', 'Cahul', 101),
(49, '15:50:00', 'Cahul', 102),
(50, '16:20:00', 'Cahul', 103),
(51, '16:45:00', 'Cahul', 104),
(52, '17:00:00', 'Cahul', 105);

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `localitati`
--

CREATE TABLE IF NOT EXISTS `localitati` (
  `Nume_loc` varchar(20) CHARACTER SET latin1 NOT NULL,
  `ds_Chis` int(10) NOT NULL,
  `ds_Cahul` int(10) NOT NULL,
  `raion` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Timp_chis` time NOT NULL,
  `Timp_cah` time NOT NULL,
  PRIMARY KEY (`Nume_loc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `localitati`
--

INSERT INTO `localitati` (`Nume_loc`, `ds_Chis`, `ds_Cahul`, `raion`, `Timp_chis`, `Timp_cah`) VALUES
('Antonesti', 112, 52, 'Cantemir', '01:54:00', '01:01:00'),
('Cahul', 164, 0, 'Cahul', '02:55:00', '00:05:00'),
('Cantemir', 120, 44, 'Cantemir', '02:00:00', '00:55:00'),
('Cazangic', 78, 86, 'Leova', '01:20:00', '01:35:00'),
('Chircani', 143, 21, 'Cahul', '02:30:00', '00:25:00'),
('Chisinau', 0, 164, 'Municipiul Chisinau', '00:05:00', '02:55:00'),
('Cneazevca', 63, 101, 'Leova', '01:05:00', '01:50:00'),
('Ghioltosu', 131, 33, 'Cantemir', '02:18:00', '00:37:00'),
('Gotesti', 136, 31, 'Cantemir', '02:25:00', '00:30:00'),
('Hanasenii Noi', 102, 62, 'Leova', '01:45:00', '01:15:00'),
('Hincesti', 36, 128, 'Hincesti', '00:25:00', '02:30:00'),
('Leova', 95, 69, 'Leova', '01:40:00', '01:20:00'),
('Mereseni', 42, 122, 'Hincesti', '00:40:00', '02:15:00'),
('Rosu', 159, 4, 'Cahul', '02:43:00', '00:13:00'),
('Sarata Noua', 83, 81, 'Leova', '01:34:00', '01:29:00'),
('Sarata-Galbena', 50, 114, 'Hincesti', '00:55:00', '02:00:00'),
('Sarateni', 66, 98, 'Leova', '01:15:00', '01:40:00'),
('Sociteni', 14, 150, 'Ialovenit', '00:20:00', '02:35:00'),
('Stoianovca', 123, 41, 'Cantemir', '02:06:00', '00:51:00'),
('Tiganca', 125, 39, 'Cantemir', '02:10:00', '00:45:00'),
('Toceni', 106, 58, 'Cantemir', '01:50:00', '01:05:00'),
('Zirnesti', 151, 13, 'Cahul', '02:35:00', '00:20:00');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `transport`
--

CREATE TABLE IF NOT EXISTS `transport` (
  `Nr_tr` int(10) NOT NULL,
  `Locuri` int(3) NOT NULL,
  `Clasa` char(1) CHARACTER SET latin1 NOT NULL,
  `An_Producere` int(5) NOT NULL,
  `Model` varchar(30) CHARACTER SET latin1 NOT NULL,
  `Caracteristici` text CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`Nr_tr`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Salvarea datelor din tabel `transport`
--

INSERT INTO `transport` (`Nr_tr`, `Locuri`, `Clasa`, `An_Producere`, `Model`, `Caracteristici`) VALUES
(101, 40, 'A', 2002, 'Iveco', 'Autobus de culoare alba'),
(102, 40, 'A', 2004, 'Iveco', 'Autobus de culoare alba'),
(103, 40, 'A', 2004, 'Iveco', 'Autobus de culoare alba'),
(104, 40, 'A', 2004, 'Iveco', 'Autobus de culoare alba'),
(105, 18, 'B', 2004, 'Mercedes', 'Microbus de culoare surie'),
(106, 18, 'B', 2000, 'Wolkswagen', 'Microbus de culoare albastra'),
(107, 18, 'B', 2001, 'Mercedes', 'Microbus de culoare alba'),
(108, 18, 'B', 2001, 'Mercedes', 'Microbus de culoare albastra'),
(109, 18, 'B', 2000, 'Wolkswagen', 'Microbus de culoare albastra'),
(110, 18, 'B', 2000, 'Mercedes', 'Microbus de culoare surie'),
(111, 18, 'B', 2002, 'Mercedes', 'Microbus de culoare alba'),
(112, 18, 'B', 2002, 'Mercedes', 'Microbus de culoare alba'),
(113, 18, 'B', 2002, 'Mercedes', 'Microbus de culoare alba'),
(114, 18, 'B', 2000, 'Wolkswagen', 'Microbus de culoare albastra'),
(115, 40, 'A', 2004, 'Iveco', 'Autobus de culoare alba'),
(116, 40, 'A', 2004, 'Iveco', 'Autobus de culoare alba'),
(117, 40, 'A', 2004, 'Iveco', 'Autobus de culoare alba'),
(118, 40, 'A', 2004, 'Iveco', 'Autobus de culoare alba'),
(119, 18, 'B', 2000, 'Wolkswagen', 'Microbus de culoare albastra'),
(120, 18, 'B', 2002, 'Mercedes', 'Microbus de culoare alba'),
(121, 18, 'B', 2002, 'Mercedes', 'Microbus de culoare alba'),
(122, 18, 'B', 2002, 'Mercedes', 'Microbus de culoare alba'),
(123, 18, 'B', 2002, 'Mercedes', 'Microbus de culoare alba'),
(124, 18, 'B', 2002, 'Mercedes', 'Microbus de culoare alba'),
(125, 18, 'B', 2002, 'Mercedes', 'Microbus de culoare alba'),
(126, 18, 'B', 2002, 'Mercedes', 'Microbus de culoare alba');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=58 ;

--
-- Salvarea datelor din tabel `user`
--

INSERT INTO `user` (`id`, `fullName`) VALUES
(53, 'Marco Pivetta'),
(54, 'Marco Pivetta'),
(55, 'Marco Pivetta'),
(56, 'Marco Pivetta'),
(57, 'Marco Pivetta');

-- --------------------------------------------------------

--
-- Structura de tabel pentru tabelul `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(30) DEFAULT NULL,
  `last_name` varchar(30) DEFAULT NULL,
  `email_address` varchar(30) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Salvarea datelor din tabel `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email_address`, `username`, `password`) VALUES
(1, 'Igor', 'Chiriac', 'igorchiriac@gmail.com', 'ichiriac', '41a8cfe1555e6bd0a854357b53ece69a'),
(2, 'Dumitru', 'Terente', 'dumitru@gmail.com', 'nterente', '25f9e794323b453885f5181f1b624d0b'),
(3, 'Dumitru', 'Terente', 'dumitru@gmail.com', 'nterente', '25f9e794323b453885f5181f1b624d0b'),
(4, 'test', 'test', 'mamamia@gmail.com', 'testtest', 'f5d1278e8109edd94e1e4197e04873b9'),
(5, 'Mihai', 'Dimici', 'test@test.com', 'MDimici', 'b8670e0f27dd25e02b570fedb66835b7'),
(6, 'Igor', 'Chiriac', 'igorchiriac1991@yahoo.com', 'igor_admin', 'e42807b0e4937cfd129a85f0c20ffa09');

--
-- Restrictii pentru tabele sterse
--

--
-- Restrictii pentru tabele `bilete`
--
ALTER TABLE `bilete`
  ADD CONSTRAINT `bilete_ibfk_1` FOREIGN KEY (`IdCursa`) REFERENCES `curse` (`IdCursa`),
  ADD CONSTRAINT `bilete_ibfk_2` FOREIGN KEY (`IdClient`) REFERENCES `clienti` (`IdClient`);

--
-- Restrictii pentru tabele `curse`
--
ALTER TABLE `curse`
  ADD CONSTRAINT `curse_ibfk_1` FOREIGN KEY (`Nume_loc`) REFERENCES `localitati` (`Nume_loc`),
  ADD CONSTRAINT `curse_ibfk_2` FOREIGN KEY (`Nr_tr`) REFERENCES `transport` (`Nr_tr`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
