<?php
class data_manip
	extends CI_Controller
{

	function get_data()
	{
		$this->load->model( 'crud_model' );
		$result_ar = array();
		$result_ar = $this->crud_model->get_curse();
		echo json_encode( $result_ar );
	}

	function get_loc()
	{
		$this->load->model( 'crud_model' );
		$result_ar = array();
		$result_ar = $this->crud_model->get_loc();
		echo json_encode( $result_ar );
	}

	function get_tra()
	{
		$this->load->model( 'crud_model' );
		$result_ar = array();
		$result_ar = $this->crud_model->get_tra();
		echo json_encode( $result_ar );
	}

}