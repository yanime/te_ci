<?php

/**
 * @property mixed input
 */
class search_form
	extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library( 'firephp' );
	}

	function index()
	{
		$this->load->view( 'include/header' );
		$this->load->view( 'search' );
		$this->load->view( 'include/footer' );
	}

	function search()
	{
		$this->load->model( 'search_model' );
		$tags1 = $this->input->post( 'tags1' );
		if( !strlen( $tags1 ) < 1 )
		{
			$rows = $this->search_model->form_search( $tags1 );
		}

		$json_array = array();

		foreach( $rows as $row )
		{
			$json_array[] = $row->nume_loc;
		}

		echo json_encode( $json_array );
	}

	function valid()
	{
		$this->load->model( 'search_model' );
		$tags1 = $this->input->post( 'tags1' );
		$tags2 = $this->input->post( 'tags2' );
		$len1 = $this->search_model->form_validation( $tags1 );
		$len2 = $this->search_model->form_validation( $tags2 );
		$arr = array();
		if( empty( $len1 ) )
		{
			$arr[] = "tag1";
		}
		if( empty( $len2 ) )
		{
			$arr[] = "tag2";
		}

		echo json_encode( $arr );
	}

	public function time_add( $timeInit, $timeAdd )
	{
		$v = $timeInit;
		$v2 = $timeAdd;
		$h = $v->format( 'H' );
		$m = $v->format( 'i' );
		$s = $v->format( 's' );
		$h2 = $v2->format( 'H' );
		$m2 = $v2->format( 'i' );
		$s2 = $v2->format( 's' );
		$s_t = $s + $s2;
		if( $s_t >= 60 )
		{
			$m_a = 1;
			$s_t = $s_t - 60;
		}
		$m_t = $m + $m2;
		if( $m_t >= 60 )
		{
			$h_a = 1;
			$m_t = $m_t - 60;
			if( isset( $m_a ) )
			{
				$m_t = $m_t + $m_a;
			}
		}
		$h_t = $h + $h2;
		if( isset( $h_a ) )
		{
			$h_t = $h_t + $h_a;
		}
		return $h_t . ':' . $m_t . ':' . $s_t;
	}

	function show_result()
	{
		$this->load->model( 'search_model' );
		$array_data = array();
		$array_data['loc_init'] = $this->input->post( 'loc_init' );
		$array_data['loc_fin'] = $this->input->post( 'loc_fin' );
		$array_data['date'] = $this->input->post( 'date' );
		$timp = date( 'H:i:s', strtotime( $this->input->post( 'time' ) ) );
		$array_r = array();

		$test = $this->search_model->loc_infor( $array_data );
		$d1 = $test['loc_init'][0]['ds_Chis'];
		$d2 = $test['loc_fin'][0]['ds_Chis'];
		$distanta = intval( $d1 ) - intval( $d2 );

		if( $d1 < $d2 )
		{
			$time = $this->search_model->timp( 'Chisinau' );
			$add_time = $this->search_model->add_timp( $array_data['loc_init'], 'Timp_chis' );
			$time1 = date( 'H:i:s', strtotime( $add_time[0]['Timp_chis'] ) );
		}
		else
		{
			$time = $this->search_model->timp( 'Cahul' );
			$add_time = $this->search_model->add_timp( $array_data['loc_init'], 'Timp_cah' );
			$time1 = date( 'H:i:s', strtotime( $add_time[0]['Timp_cah'] ) );
		};
		$t1 = new DateTime( $time1 );
		$locTime_ar = array();
		$id_ar = array();
		foreach( $time as $val )
		{
			$id_ar[$val['IdCursa']] = $val['ora_Prn'];
		}
		foreach( $id_ar as $k => $v )
		{
			$t = date( 'H:i:s', strtotime( $v ) );
			$t2 = new DateTime( $t );
			$locTime_ar[$k] = $this->time_add( $t2, $t1 );
		}
		$tmU = date( 'H:i:s', strtotime( $timp ) );
		$tm3 = new DateTime( $tmU );
		foreach( $locTime_ar as $k => $time_loc )
		{
			$tm = date( 'H:i:s', strtotime( $time_loc ) );
			$tm2 = new DateTime( $tm );
			$interval = $tm3->diff( $tm2 );
			if( $tm2 > $tm3 )
			{
				$tue = $interval->format( '%h' );
			}
			if( isset( $tue ) && $tue < 3 )
			{
				$arr = array();
				$arr['ora'] = $tm2->format( "H:i:s" );
				$arr['pret'] = abs( $distanta ) * 0.36;
				$array_r[$k] = $arr;
			}
		}
		echo json_encode( $array_r );
	}
}