<?php
class admin_page
	extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->is_logged_in();
	}

	function  index()
	{
		$this->load->view( 'include/header' );
		$this->load->view( 'control_page' );
		$this->load->view( 'include/footer' );
	}

	function control_page()
	{
		$this->load->view( 'include/header' );
		$this->load->view( 'control_page' );
		$this->load->view( 'include/footer' );
		$this->session->userdata( 'session_id' );
	}

	function is_logged_in()
	{
		$is_logged_in = $this->session->userdata( 'is_logged_in' );
		if( !isset( $is_logged_in ) || $is_logged_in != true )
		{
			echo 'You dont have permission <a href="../login">Login</a>';
			die();
		}
	}

	function set_place()
	{

	}

}

?>
