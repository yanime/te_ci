<?php

class pdf
	extends CI_Controller
{

	public $load;

	public function __construct()
	{
		parent::FPDF();
	}

	function index()
	{
		$this->load->library( 'fpdf' );
	}

}

class pdf_mail
	extends FPDF
{

	function SetReportFirstPageHead( $report_name, $print_date, $optional_text = '' )
	{
		//$this->Image('logo.png', 20, 13, 60, 0, '');
		$this->SetFont( 'Arial', '', 15 );
		$this->Cell( 170, 10, 'abc tourism,', '', 0, 'R' );
		$this->Ln( 7 );
		$this->SetFont( 'Arial', '', 12 );
		$this->Cell( 170, 10, 'address 1,', '', 0, 'R' );
		$this->Ln( 5 );
		$this->Cell( 170, 10, 'address 2,', '', 0, 'R' );
		$this->Ln( 5 );
		$this->Cell( 170, 10, 'address 3 ', '', 0, 'R' );
		$this->Ln( 7 );
		$this->SetFont( 'Arial', '', 10 );
		$this->Cell( 170, 10, 'phone', '', 0, 'R' );
		$this->Ln( 5 );
		$this->Cell( 170, 10, 'mail', '', 0, 'R' );
		$this->Line( 20, 48, 190, 48 );
		$this->Ln( 9 );

		$this->SetFont( 'Arial', '', 12 );
		$this->Cell( 140, 8, $report_name, '', 0, 'L' );
		$this->SetFont( 'Arial', '', 10 );
		$this->Cell( 30, 9, "Printed On: " . $print_date, '', 0, 'R' );

		$this->Line( 20, 55, 190, 55 );

		if( $optional_text != '' )
		{
			$this->Ln( 10 );
			//$this->WriteHTML($optional_text);
			$this->Ln( 15 );
		}
		else
		{
			$this->Ln( 15 );
		}
	}

	function SetReportGeneralPageHead( $report_name, $print_date )
	{
		$this->SetFont( 'Arial', '', 11 );
		$this->Cell( 140, 8, $report_name, '', 0, 'L' );
		$this->SetFont( 'Arial', '', 9 );
		$this->Cell( 30, 9, $print_date, '', 0, 'R' );
		$this->Line( 20, 20, 190, 20 );

		$this->Ln( 15 );
	}

}