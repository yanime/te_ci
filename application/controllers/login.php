<?php

class Login
	extends CI_Controller
{

	function index()
	{
		$data['main_content'] = 'login_form';
		$this->load->view( 'include/header' );
		$this->load->view( 'include/template', $data );
		$this->load->view( 'include/footer' );

	}

	function validate_input()
	{
		$this->load->model( 'member_model' );
		$query = $this->member_model->validate();

		if( $query )
		{
			$data = array(
				'username' => $this->input->post( 'username' ),
				'is_logged_in' => true
			);
			$this->session->set_userdata( $data );
			redirect( 'admin_page/control_page' );
		}
		else
		{
			$this->index();
		}
	}

	function signup()
	{
		$data['main_content'] = 'signup_form';
		$this->load->view( 'include/header' );
		$this->load->view( 'include/template', $data );
		$this->load->view( 'include/footer' );
	}

	function create_member()
	{
		$this->load->library( 'form_validation' );
		//field name,error message,validation rules
		$this->form_validation->set_rules( 'first_name', 'Name', 'trim|required' );
		$this->form_validation->set_rules( 'last_name', 'Last Name', 'trim|required' );
		$this->form_validation->set_rules( 'email_address', 'Email Address', 'trimrequired|valid_email' );

		$this->form_validation->set_rules( 'username', 'Username', 'trim|required|min_length[4]' );
		$this->form_validation->set_rules( 'password', 'Password', 'trim|required|min_length[6]' );
		$this->form_validation->set_rules( 'password2', 'Password confirmation', 'trim|required|matches[password]' );

		if( $this->form_validation->run() == FALSE )
		{
			$this->signup();

		}
		else
		{
			$this->load->model( 'member_model' );
			if( $query = $this->member_model->create_member() )
			{
				$data['main_content'] = 'signup_successful';
				$this->load->view( 'include/template', $data );
			}
			else
			{
				$this->load->view( 'signup_form' );
			}
		}

	}

}

?>
