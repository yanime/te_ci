<?php

class Save_data
	extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library( 'firephp' );
		$this->load->library( 'pdf' );
	}

	function available()
	{
		$response = array();
		$array_data['IdCursa'] = $this->input->post( 'idcursa' );
		$array_data['date'] = $this->input->post( 'date' );
		$this->load->model( 'save_data_model' );
		$response = $this->save_data_model->place( $array_data );
		echo json_encode( $response );
	}

	function saveClient()
	{
		$params = array();
		$params['nume'] = $this->input->post( 'name' );
		$params['prenume'] = $this->input->post( 'prename' );
		$params['email'] = $this->input->post( 'email' );

		$this->load->model( 'save_data_model' );
		$respons = $this->save_data_model->check_client( $params['email'] );

		if( !empty( $respons ) )
		{
			$this->saveTicket( $respons[0]['IdClient'] );
		}
		else
		{
			$this->load->model( 'save_data_model' );
			$respons = $this->save_data_model->save_client( $params );
			if( !empty( $respons ) )
			{
				$this->saveTicket( $respons );
			}
		}
	}

	function create_pdf( $array_data )
	{

		$this->load->library( 'pdf' );
		$this->load->library( 'fpdf' );
		$pdf = new PDF();

		$header = array( 'Loc. pornire', 'Localitate oprire', 'Loc', 'Data', 'Ora', 'Pret' );

		//First page
		$pdf->AddPage();
		$pdf->SetLeftMargin( 20 );
		$pdf->SetReportFirstPageHead( 'Date de calatorie', date( 'F j, Y' ) );
		$pdf->FancyTable( $header, $array_data );

		$pdf->Output( 'abc.pdf', 'F' );
	}

	function send_email()
	{

		$params['nume'] = $this->input->post( 'name' );
		$params['prenume'] = $this->input->post( 'prename' );
		$params['email'] = $this->input->post( 'email' );

		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'igorchiriac1991@gmail.com',
			'smtp_pass' => 'tractorash8801'
		);
		$this->load->library( 'email', $config );
		$this->email->set_newline( "\r\n" ); /* for some reason it is needed */
		$email_body = "Multumim " . $params['nume'] . " pentru rezervarea efectuata     ";
		$email_body .= "Aveti atasat un fisier cu toate datele de calatorie";
		$this->email->from( 'igorchiriac1991@gmail.com', 'Igor Chiriac' );
		$this->email->to( $params['email'] );
		$this->email->subject( 'Bilet de calatorie' . $params['nume'] . " " . $params['prenume'] );
		$this->email->attach( 'abc.pdf' );
		$this->email->message( $email_body );
		if( $this->email->send() )
		{
			return true;
		}
		else
		{
			show_error( $this->email->print_debugger() );
		}
	}

	function saveTicket( $idClient )
	{
		$response = array();
		$locuri = array();
		$resp = array();

		$array_data['serie'] = 'A';
		$array_data['data_cursa'] = $this->input->post( 'data' );
		$array_data['ora_cursa'] = $this->input->post( 'ora' );
		$array_data['p_initial'] = $this->input->post( 'tags1' );
		$array_data['p_final'] = $this->input->post( 'tags2' );
		$array_data['pret'] = $this->input->post( 'pret' );
		$places = $this->input->post( 'places' );
		$array_data['IdCursa'] = $this->input->post( 'idcursa' );
		$array_data['IdClient'] = $idClient;
		$locuri[] = explode( ',', $places );

		$this->load->model( 'save_data_model' );
		foreach( $locuri[0] as $place )
		{
			$array_data['loc'] = $place;
			$response[] = $array_data;
			$resp['save_ticket'] = $this->save_data_model->save_ticket( $array_data );
		}

		$this->create_pdf( $response );
		if( $this->send_email() )
		{
			$resp['email'] = 'trimis';
		}

		echo json_encode( $resp );
	}

	function succes()
	{
		$this->load->view( 'include/header' );
		$this->load->view( 'succes_view' );

	}

}