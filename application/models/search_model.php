<?php

class search_model
	extends CI_Model
{

	public function form_search( $tag )
	{
		$this->db->select( 'nume_loc' );
		$this->db->like( 'nume_loc', $tag, 'after' );
		$query = $this->db->get( 'localitati' );
		return $query->result();
	}

	public function form_validation( $tag )
	{
		$this->db->select( 'nume_loc' );
		$this->db->where( 'nume_loc', $tag );
		$query = $this->db->get( 'localitati' );
		return $query->result();
	}

	public function loc_infor( $params )
	{
		$loc_init = $params['loc_init'];
		$loc_fin = $params['loc_fin'];
		$param = array();
		$query = $this->db->query( "select ds_Chis,timp_chis from localitati where Nume_loc='$loc_init'" );
		$param['loc_init'] = $query->result_array();
		$query = $this->db->query( "select ds_Chis,timp_chis from localitati where Nume_loc='$loc_fin'" );
		$param['loc_fin'] = $query->result_array();
		return $param;
	}

	public function timp( $loc )
	{
		$query = $this->db->query( "select IdCursa,ora_Prn from curse where Nume_loc='$loc'ORDER BY `curse`.`Ora_Prn` ASC" );
		return $query->result_array();
	}

	public function add_timp( $param, $loc )
	{
		$query = $this->db->query( "select $loc from localitati where Nume_loc='$param'" );
		return $query->result_array();
	}

}

