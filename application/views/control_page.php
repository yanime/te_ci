<meta charset="utf-8">
<title>Admin panel</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="date,client,angajat">
<meta name="author" content="Chiriac Igor">
<!-- Le styles -->
<style type="text/css">
body
{
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav
{
	padding: 9px 0;
}

@media (max-width: 980px)
{
	/* Enable use of floated navbar text */
	.navbar-text.pull-right
	{
		float: none;
		padding-left: 5px;
		padding-right: 5px;
	}
}
</style>
<script>
$( document ).ready( function() {
	$( '#curse' ).click( function() {
		// prepare the data
		var source =
		{
			datatype : "json",
			datafields : [
				{ name : 'IdCursa', type : 'int'},
				{ name : 'Ora_Prn', type : 'time' },
				{ name : 'Nume_loc' },
				{ name : 'Nr_tr', type : 'int' }
			],
			id : 'id',
			url : '../data_manip/get_data',
			root : 'data'
		};
		var dataAdapter = new $.jqx.dataAdapter( source );
		var cellsrenderer = function( row, columnfield, value, defaulthtml, columnproperties ) {
			if( value < 20 ) {
				return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #ff0000;">' + value + '</span>';
			}
			else {
				return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #008000;">' + value + '</span>';
			}
		}

		// initialize jqxGrid
		$( "#jqxgrid" ).jqxGrid(
			{
				width : 800,
				source : dataAdapter,
				pageable : true,
				autoheight : true,
				sortable : true,
				altrows : true,
				enabletooltips : true,
				editable : true,
				selectionmode : 'multiplecellsadvanced',
				columns : [
					{ text : 'Id Cursa', datafield : 'IdCursa', width : 200 },
					{ text : 'Ora pornire', datafield : 'Ora_Prn', cellsalign : 'left', align : 'left', width : 200 },
					{ text : 'Nume localitate', datafield : 'Nume_loc', align : 'left', cellsalign : '', cellsformat : 'c2', width : 200 },
					{ text : 'Numar transport', datafield : 'Nr_tr', cellsalign : 'left', cellsrenderer : cellsrenderer, width : 200 }
				]
			} );
	} );
	$( '#local' ).click( function() {
		var theme = getDemoTheme();
		// prepare the data
		var source =
		{
			datatype : "json",
			datafields : [
				{ name : 'Nume_loc' },
				{ name : 'ds_Chis' },
				{ name : 'ds_Cahul' },
				{ name : 'raion'}
			],
			id : 'id',
			url : '../data_manip/get_loc',
			root : 'data'
		};
		var dataAdapter = new $.jqx.dataAdapter( source );
		var cellsrenderer = function( row, columnfield, value, defaulthtml, columnproperties ) {
			if( value < 20 ) {
				return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #ff0000;">' + value + '</span>';
			}
			else {
				return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #008000;">' + value + '</span>';
			}
		}
		// prepare the dat

		// initialize jqxGrid
		$( "#jqxgrid" ).jqxGrid(
			{
				width : 800,
				source : dataAdapter,
				theme : theme,
				pageable : true,
				autoheight : true,
				sortable : true,
				altrows : true,
				enabletooltips : true,
				editable : true,
				selectionmode : 'multiplecellsadvanced',
				columns : [
					{ text : 'Nume localitate', datafield : 'Nume_loc', align : 'left', cellsalign : '', cellsformat : 'c2', width : 200  },
					{ text : 'Distanta Chisinau', datafield : 'ds_Chis', cellsalign : 'left', align : 'left', width : 200 },
					{ text : 'Distanta Cahul', datafield : 'ds_Cahul', align : 'left', cellsalign : '', cellsformat : 'c2', width : 200 },
					{ text : 'Raion', datafield : 'raion', cellsalign : 'left', cellsrenderer : cellsrenderer, width : 200 }
				]
			} );
	} );
	$( '#transport' ).click( function() {
		var theme = getDemoTheme();
		// prepare the data
		var source =
		{
			datatype : "json",
			datafields : [
				{ name : 'Nr_tr' },
				{ name : 'Locuri' },
				{ name : 'Clasa' },
				{ name : 'An_Producere'},
				{ name : 'Model'}
			],
			id : 'id',
			url : '../data_manip/get_tra',
			root : 'data'
		};
		var dataAdapter = new $.jqx.dataAdapter( source );
		var cellsrenderer = function( row, columnfield, value, defaulthtml, columnproperties ) {
			if( value < 20 ) {
				return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #ff0000;">' + value + '</span>';
			}
			else {
				return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: #008000;">' + value + '</span>';
			}
		}
		// prepare the dat

		// initialize jqxGrid
		$( "#jqxgrid" ).jqxGrid(
			{
				width : 800,
				source : dataAdapter,
				theme : theme,
				pageable : true,
				autoheight : true,
				sortable : true,
				altrows : true,
				enabletooltips : true,
				editable : true,
				selectionmode : 'multiplecellsadvanced',
				columns : [
					{ text : 'Numarul', datafield : 'Nr_tr', align : 'left', cellsalign : '', cellsformat : 'c2', width : 200  },
					{ text : 'Numarul de locuri', datafield : 'Locuri', cellsalign : 'left', align : 'left', width : 200 },
					{ text : 'Clasa', datafield : 'Clasa', align : 'left', cellsalign : '', cellsformat : 'c2', width : 200 },
					{ text : 'An producere', datafield : 'An_Producere', cellsalign : 'left', cellsrenderer : cellsrenderer, width : 200 },
					{ text : 'Model', datafield : 'Model', cellsalign : 'left', cellsrenderer : cellsrenderer, width : 200 }
				]
			} );
	} );
} );
</script>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="brand" href="#">BookTicket</a>
			<div class="nav-collapse collapse">
				<p class="navbar-text pull-right">
					Logged in as <a href="#" class="navbar-link"><?= $this->session->userdata( 'username' ); ?></a>
				</p>
				<ul class="nav">
					<li class="active"><a href="#">Home</a></li>
					<li><a href="#about">About</a></li>
					<li><a href="#contact">Contact</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span2">
			<div class="well sidebar-nav">
				<ul class="nav nav-list">
					<li class="nav-header">Client date</li>
					<li><a href="#" id='bilete'>Bilete</a></li>
					<li><a href="#" id="curse">Curse</a></li>
					<li><a href="#" id="local">Localitati</a></li>
					<li><a href="#" id="transport">Transport</a></li>
					<li class="nav-header">Angajat date</li>
					<li><a href="#">Angajati</a></li>
					<li><a href="#">Venituri</a></li>
				</ul>
			</div>
			<!--/.well -->
		</div>
		<!--/span-->
		<div class="span9">
			<div class="hero-unit">
				<div id='jqxWidget' style="font-size: 13px; font-family: Verdana; float: left;">
					<div id="jqxgrid"></div>
				</div>
			</div>
		</div>
		<!--/span-->
	</div>
	<!--/row-->
	<hr>
	<footer>
		<p>&copy; Company 2013</p>
	</footer>
</div>
<!--/.fluid-container-->
<!-- Le javascript
 ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
