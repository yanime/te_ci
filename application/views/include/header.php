<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<title>Bus-Tickets</title>
<link href="<?php echo base_url( 'assets/css/bootstrap-select.min.css' ) ?>" rel="stylesheet">
<link href="<?php echo base_url( 'assets/css/bootstrap-responsive.min.css' ) ?>" rel="stylesheet">
<link href="<?php echo base_url( 'assets/css/datepicker.css' ) ?>" rel="stylesheet">
<link href="<?php echo base_url( 'assets/css/global.css' ) ?>" rel="stylesheet">
<link href="<?php echo base_url( 'assets/css/flick/jquery-ui-1.10.3.custom.min.css' ) ?>" rel="stylesheet">
<link rel="icon" href="<?php echo base_url( 'assets/ico/bus.ico' ) ?>">
<link href="<?php echo base_url( 'assets/css/bootstrap.css' ) ?>" rel="stylesheet">
<link href="<?php echo base_url( 'assets/css/font_awesome.css' ) ?>" rel="stylesheet">
<script type="text/javascript" language="javascript" charset="utf-8" src="<?php echo base_url( 'assets/js/jquery-1.9.1.js' ) ?>"></script>
<script type="text/javascript" language="javascript" charset="utf-8" src="<?php echo base_url( 'assets/js/maskedit.js' ) ?>"></script>
<script type="text/javascript" language="javascript" charset="utf-8" src="<?php echo base_url( 'assets/js/bootstrap.min.js' ) ?>"></script>
<script type="text/javascript" language="javascript" charset="utf-8" src="<?php echo base_url( 'assets/js/bootstrap-select.min.js' ) ?>"></script>
<script type="text/javascript" language="javascript" charset="utf-8" src="<?php echo base_url( 'assets/js/bootstrap-datepicker.js' ) ?>"></script>
<script type="text/javascript" language="javascript" charset="utf-8" src="<?php echo base_url( 'assets/js/jquery-ui-1.10.3.custom.min' ) ?>"></script>
<script type="text/javascript" language="javascript" charset="utf-8" src="<?php echo base_url( 'assets/js/spinner.min.js' ) ?>"></script>
<script type="text/javascript" language="javascript" charset="utf-8" src="<?php echo base_url( 'assets/js/main.js' ) ?>"></script>
</head>
<body>
<nav id="navigation">
	<div class="container">
		<ul class="navlinks">
			<li><a href="index.html">Homepage</a></li>
			<li><a href="index.html">About Us</a></li>
			<li><a href="index.html">Projects</a></li>
			<li><a href="index.html">The Team</a></li>
			<li><a href="index.html">Contacts</a></li>
		</ul>
	</div>
</nav>
