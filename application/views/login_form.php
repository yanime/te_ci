<?php
$attributes = array( 'class' => 'well span3',
	'id' => 'myform' );
$opt = array( 'class' => 'btn btn-primary' );
echo form_open( 'login/validate_input', $attributes );?>
	<h2>Logare in sistem</h2>

	<label class="text-success">Username</label>
<?php echo form_input( 'username' );
echo "</br>" ?>
	<label class="text-success">Password</label>
<?php echo form_password( 'password' );
echo form_submit( $opt, 'submit', 'Logare!' );
echo anchor( 'login/signup', 'Creare account' );
?>