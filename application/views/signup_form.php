<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ichiriac
 * Date: 3/26/13
 * Time: 2:24 PM
 * To change this template use File | Settings | File Templates.
 */
?>
<h1>Inregistrare</h1>
<fieldset>
	<legend>Informatie personala</legend>
	<?php
	$attributes = array( 'class' => 'well span3',
		'id' => 'myform' );
	echo form_open( 'login/create_member', $attributes );

	echo form_input( 'first_name', set_value( 'first_name', 'First Name' ) );
	echo form_input( 'last_name', set_value( 'last_name', 'Last Name' ) );
	echo form_input( 'email_address', set_value( 'email_address', 'Email address' ) );
	?>
	<legend>Login info</legend>
	<?php
	$attribute = array( 'class' => 'well span3',
		'id' => 'signup' );
	echo form_open( 'login/validate_input', $attribute );
	echo form_input( 'username', set_value( 'username', 'Username' ) );
	echo form_password( 'password', set_value( 'password', 'Password' ) );
	echo form_password( 'password2', set_value( 'password2', 'Password2' ) );
	echo form_submit( 'submit', 'Sign up!' );
	?>

	<?php echo validation_errors( '<p class="error">' ); ?>
