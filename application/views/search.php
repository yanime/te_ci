<header id="heading">
	<?php
	$attributes = array(
		'class' => 'search_form',
		'id' => 'search',
		'method' => 'post',
		'onsubmit' => 'return check()'
	);
	echo form_open( 'search_form/valid', $attributes );
	?>
	<div class="container text-center">
		<h2>Rezerveaza bilet de calatorie</h2>
		<div class='container'>
			<div class="row-fluid">
				<div class="span7" id="div1"></div>
				<div class=" span7" id="form">
					<div class="row-fluid">
						<div class="span3">
							<a class="btn btn-primary" href="#" id="mod_caut">
								<i class="icon-refresh icon-spin "></i> Modifica cautarea </a>
						</div>
						<div class="span5"><p class="text-error" id="ErrorMes">Completati toate casutele cu informatiile necesare <i
									class="icon-exclamation"></i></p></div>
					</div>
					<div class="row-fluid" id="firstdiv">
						<div class="span4">
							<label class="labl1" id="l1">Localitatea pornire:</label>
							<input id="tags1" type='text' name="tags1" class="name_loc" data-provide="typeahead" data-items="4" placeholder="Chisinau"
							       required/>
						</div>
						<div class="span4">
							<label class="labl1" id="l2"> Localitate oprire:</label>
							<input id="tags2" type="text" name="tags2" class="name_loc" data-provide="typeahead" data-items="4" placeholder="Cantemir"
							       required/>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span2">
							<label class="labelname" id="labl3">Adulti</label>
							<select name="adulti" id="n_adult" class="selectpicker" data-width="35%">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
						<div id="select" class="span2">
							<label class="labelname" id="labl4">Copii</label>
							<select name="copiii" id="n_copiii" class="selectpicker" data-width="35%">
								<option value="0">--</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
							</br>
						</div>
						<div class="span3">
							<label class="labelname" id="data">Data calatoriei:</label>
							<input type="text" id="datepicker" name='data' required/>
						</div>
						<div class="span2">
							<label class="labelname" id='ora'>Ora</label>
							<input name="time" type='text' id="time"/>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span4 offset5">
							<button class="btn btn-large btn-inverse search" id="send" type="button">Search <i class="icon-circle-arrow-right"></i></button>
						</div>
						<div class="span3"><?php echo validation_errors( '<p class="error">' ); ?></div>
					</div>
				</div>
				<div id="search_result" class="row-fluid">
					<div class="span7">
						<div id="result">
							<table id="t_result" class="table table-hover">
								<thead>
								<tr>
									<td>Plecarea</td>
									<td>Sosirea</td>
									<td>Data</td>
									<td>Ora</td>
									<td>Pret</td>
								</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<div class="span6 offset3">
							<button class="btn btn-large btn btn-success" id="saveData" type="button">Finalizeaza comanda</button>
						</div>
					</div>
					<div id="bus_map" class="span5">
						<div class="row-fluid">Selecteaza cursa pentru a vedea locurile disponibile</div>
						<div id="seats" class="span3">
							<ul id="place">
							</ul>
						</div>
						<div class="row-fluid" id="legend">
							<div class="span2">
								<img src="assets/img/available_seat_img.gif" class="img-rounded">
								Loc liber
							</div>
							<div class="span3"><img src="assets/img/booked_seat_img.gif" class="img-rounded">
								Loc ocupat
							</div>
							<div class="span3"><img src="assets/img/selected_seat_img.gif" class="img-rounded">
								Loc selectat
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="dialog-form" title="Salvarea datelor" style="display:none">
				<p id="message"><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>
				</p>
				<p class="validateTips">Toate cimpurile sunt obligatorii.</p>
				<form>
					<fieldset style="margin-left: 30px">
						<label for="name">Nume :
							<input type="text" style="margin-left: 20px;" name="name" id="name" class="text ui-widget-content ui-corner-all test"/></label>
						<label for="prename">Prenume :
							<input type="text" name="password" id="prename" value="" class="text ui-widget-content ui-corner-all test"/></label>
						<label for="email">Email :
							<input type="text" style="margin-left: 20px;" name="email" id="email" value=""
							       class="text ui-widget-content ui-corner-all test"/></label>
					</fieldset>
				</form>
			</div>
</header>
