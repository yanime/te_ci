<?php

require('fpdf.php');

class PDF extends FPDF
{
 //Page header
function Header()
{
    // Logo
    //$this->Image('logo.png',10,6,30);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(50,10,'Bilet de calatorie',1,0,'C');
    // Line break
    $this->Ln(20);
}

// Page footer
function Footer()
{
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}

function SetReportFirstPageHead($report_name, $print_date, $optional_text = '')
{
      //$this->Image('logo.png',20,13,60,0,''); 
      $this->SetFont('Arial','',15);
      $this->Cell(170,10,'Trans-S.R.L,','',0,'R');
      $this->Ln(7);
      $this->SetFont('Arial','',12);
      $this->Cell(170,10,'or. Cahul , str. Trandafirilor 20/1,','',0,'R');
      $this->Ln(7);
      $this->SetFont('Arial','',10);
      $this->Cell(170,10,'Tel. 079555444','',0,'R');
      $this->Ln(5);
      $this->Cell(170,10,'email : trans@gmail.com','',0,'R');
      $this->Ln(9);

      $this->SetFont('Arial','',12);
      $this->Cell(140,8,$report_name,'',0,'L');
      $this->SetFont('Arial','',10);
      $this->Cell(30,9, "Printat la data de : ".$print_date,'',0,'R');

      $this->Line(20, 58, 190, 58);

      if ($optional_text != '')
      {
        $this->Ln(10);
       // $this->WriteHTML($optional_text);
        $this->Ln(15);
      }
      else 
      {
        $this->Ln(15);
      }
}
function FancyTable($header,$data)
{
    //Colors, line width and bold font
    $this->SetFillColor(255,0,0);
    $this->SetTextColor(255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.3);
    $this->SetFont('','B');
    //Header
    $w=array(35,35,25,30,25,25);
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],8,$header[$i],1,0,'C',1);
    $this->Ln();
    //Color and font restoration
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    //Data
    $fill=0;
    foreach($data as $row)
    {
        $this->Cell($w[0],7,$row['p_initial'],'LR',0,'L',$fill);
        $this->Cell($w[1],7,$row['p_final'],'LR',0,'L',$fill);
        $this->Cell($w[2],7,number_format($row['loc']),'LR',0,'R',$fill);
        $this->Cell($w[3],7,$row['data_cursa'],'LR',0,'R',$fill);
        $this->Cell($w[4],7,$row['ora_cursa'],'LR',0,'R',$fill);
        $this->Cell($w[5],7,$row['pret'],'LR',0,'R',$fill);
        $this->Ln();
        $fill=!$fill;
    }
    $this->Cell(array_sum($w),0,'','T');
}
}