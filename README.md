## Denumirea

"Online booking system for bus trip"

## Acopeririea

Pentru inceput aplicatia este destinata pentru regiunea de sud a tarii.
## Documentation
Application : Book bus ticket

Used tools :

Codeigniter - php framework,
Jquery - javascript framework version 1.9,
Twitter Boostrap - front-end framework version 2.0
Boostrap-datepicker : http://eternicode.github.io/bootstrap-datepicker/
Boostrap-Selectbox : http://silviomoreto.github.io/bootstrap-select/
Boostrap-Fonts and icons : http://fortawesome.github.io/Font-Awesome/examples/
Boostrap resources : http://bootsnipp.com/resources#4
Jquery autocomplete with flick theme http://jqueryui.com/autocomplete/
MaskEdit input : http://digitalbush.com/projects/masked-input-plugin/

## Dezvoltator

Igor Chiriac
