var currentTime = new Date();
var rowId;
var ora;
var pret;
$(document).ready(function() {
    $('.selectpicker').selectpicker();
    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd',
        startDate: currentTime,
        autoclose: true,
        orientation: 'top'
    });
    $(".email").hide();
    $("#time").mask("99:99");

    //autocomplete function
    $('.name_loc').keyup(function(e) {
        if (e.which == 13) {
            e.preventDefault();
        }
        var inputId = $("*:focus").attr("id");
        var searched = $('#' + inputId).val();

        $.ajax({
            type: "POST",
            url: "search_form/search",
            data: {tags1: searched},
            dataType: "json",
            success: function(data) {
                var elements = new Array();
                if (elements.length > 0)
                    elements.length = 0;
                $.each(data, function(i, val) {
                    elements.push(val);
                });
                $('.name_loc').autocomplete({source: elements});
            }
        });
    });


    //search with validation data
    $('#ErrorMes').hide();
    $('#send').click(function() {

        var tag1 = $("#tags1").val();
        var tag2 = $("#tags2").val();
        var date = $('#datepicker').val();
        var n_cop = $("#n_copiii").val();
        var n_adult = $("#n_adult").val();
        var time = $("#time").val();
        var bool = false;
        var settings = {
            rows: 6,
            cols: 3,
            rowCssPrefix: 'row-',
            colCssPrefix: 'col-',
            seatWidth: 40,
            seatHeight: 40,
            seatCss: 'seat',
            selectedSeatCss: 'selectedSeat',
            selectingSeatCss: 'selectingSeat'
        };

        $('#l1').css("color", 'white');
        $('#l2').css("color", 'white');

        if (tag1.length == 0) {
            $('#ErrorMes').show();
            $("#ErrorMes").html("<div class='alert alert-danger'>Introduceti localitatea de pornire</div>");
            $('#l2').css("color", 'white');
            $('#l1').css("color", 'yellow');
        } else if (tag2.length == 0) {
            $('#ErrorMes').show();
            $("#ErrorMes").html("<div class='alert alert-danger'>Introduceti localitatea de oprire</div>");
            $('#l1').css("color", 'white');
            $('#l2').css("color", 'yellow');
        } else if (time.length == 0) {
            $('#l1').css("color", 'white');
            $('#l2').css("color", 'white');
            $('#ErrorMes').show();
            $("#ErrorMes").html("<div class='alert alert-danger'>Introduceti ora pornirii</div>");
            $('#ora').css("color", 'yellow');
        } else if (date.length == 0) {
            $('#l1').css("color", 'white');
            $('#l2').css("color", 'white');
            $('#ora').css("color", 'white');
            $('#data').css("color", 'yellow');
            $("#ErrorMes").html("<div class='alert alert-danger'>Introduceti data cursei</div>");
            $('#ErrorMes').show();
        } else if (tag1 === tag2) {
            $('#ErrorMes').show();
            $("#ErrorMes").html("<div class='alert alert-danger'>Introduceti nume de localitari distincte</div>");
            $('#data').css("color", 'white');
            $('#l1').css("color", 'yellow');
            $('#l2').css("color", 'yellow');
        } else if (time.slice(0, 2) > 24 || time.slice(3, 5) > 60) {
            $('#ErrorMes').show();
            $("#ErrorMes").html("<div class='alert alert-danger'>Introduceti ora corecta</div>");
            $('#ora').css("color", 'yellow');
        } else {
            $('#l1').css("color", 'white');
            $('#l2').css("color", 'white');
            $('#ErrorMes').hide();
            $('#ora').css("color", 'white');
            $('#data').css("color", 'white');
            $.ajax({
                type: "POST",
                url: "search_form/valid",
                data: {tags1: tag1, tags2: tag2},
                dataType: "json",
                success: function(data) {
                    fields = new Array();
                    $.each(data, function(key, val) {
                        fields.push(val);
                    });
                    for (i = 0; i <= fields.length; i++) {
                        if (fields[i] == "tag1") {
                            $('#l1').css("color", 'yellow');
                            $('#ErrorMes').show();
                            $("#ErrorMes").html("<div class='alert alert-danger'>Introduceti nume de localitate valida</div>");
                            bool = true;
                        }
                        if (fields[i] == "tag2") {
                            $('#l2').css("color", 'yellow');
                            $('#ErrorMes').show();
                            $("#ErrorMes").html("<div class='alert alert-danger'>Introduceti nume de localitate valida</div>");
                            bool = true
                        }
                    }
                    if (!bool) {
                        $('#l1').css("color", 'white');
                        $('#l2').css("color", 'white');
                        $('#ErrorMes').hide();
                        $.ajax({
                            type: "POST",
                            url: "search_form/show_result",
                            data: {loc_init: tag1, loc_fin: tag2, date: date, time: time},
                            dataType: "json",
                            success: function(data) {
                                var infCursa = new Array();
                                var id = new Array();
                                $.each(data, function(key, val) {
                                    infCursa.push(val);
                                    id.push(key);
                                });

                                for (i = 0; i < infCursa.length; i++) {
                                    $('table tbody').append(
                                            $('<tr id="' + id[i] + '" class="edit_tr" changed="false" ora="' + infCursa[i]['ora'] + '"pret="' + infCursa[i]['pret'] + '">' +
                                            '<td class="edit_td">' +
                                            '<span class="text">' + tag1 + '</span>' +
                                            '</td>' +
                                            '<td class="edit_td">' +
                                            '<span class="text">' + tag2 + '</span>' +
                                            '</td>' +
                                            '<td class="edit_td">' +
                                            '<span class="text">' + date + '</span>' +
                                            '</td>' +
                                            '<td class="edit_td" id="' + i + '">' +
                                            '<span class="text">' + infCursa[i]['ora'] + ' </span>' +
                                            '</td>' +
                                            '<td class="edit_td">' +
                                            '<span class="text">' + infCursa[i]['pret'] + " lei" + ' </span>' +
                                            '</td>' +
                                            '</tr>'
                                            ));
                                }
                                $('#mod_caut').show();

                            }

                        });
                        $("#search_result").toggle('clip');
                    }
                },
                error: function(data) {
                    alert('Load was performed.');
                }
            });
        }
        ;
        $("table").on('click', '.edit_tr', function() {
            $('tr').removeClass('active_tr');
            $(this).addClass('active_tr');
            rowId = $(this).closest('tr').attr('id');
            var date = $('#datepicker').val();
            ora = $(this).closest('tr').attr('ora');
            pret = $(this).closest('tr').attr('pret');
            $.ajax({
                type: "POST",
                url: "save_data/available",
                data: {date: date, idcursa: rowId},
                dataType: "json",
                success: function(data) {
                    locuri = new Array;
                    for (i = 0; i < data.length; i++) {
                        locuri.push(parseInt(data[i].loc));
                    }
                    var str = [], seatNo = 0, className;
                    for (i = 0; i < settings.rows; i++) {
                        for (j = 0; j < settings.cols; j++) {
                            seatNo++;
                            if ($.inArray(seatNo, locuri) !== -1)
                            {
                                if (j == 2) {
                                    className = settings.seatCss + ' ' + settings.rowCssPrefix + i.toString() + ' ' + settings.colCssPrefix + j.toString() + ' space' + ' ' + settings.selectedSeatCss;
                                } else {
                                    className = settings.seatCss + ' ' + settings.rowCssPrefix + i.toString() + ' ' + settings.colCssPrefix + j.toString() + ' ' + settings.selectedSeatCss;
                                }
                            } else {
                                if (j == 2) {
                                    className = settings.seatCss + ' ' + settings.rowCssPrefix + i.toString() + ' ' + settings.colCssPrefix + j.toString() + ' space';
                                } else {
                                    className = settings.seatCss + ' ' + settings.rowCssPrefix + i.toString() + ' ' + settings.colCssPrefix + j.toString();
                                }
                            }

                            str.push('<li id="' + seatNo + '"class="' + className + '"' +
                                    'style="top:' + (i * settings.seatHeight).toString() + 'px;left:' + (j * settings.seatWidth).toString() + 'px">' +
                                    '<a title="' + seatNo + '">' + seatNo + '</a>' +
                                    '</li>');
                        }
                    }
                    $('#place').html(str.join(''));

                    $('.' + settings.seatCss).on('click', function() {
                        if ($(this).hasClass(settings.selectedSeatCss)) {
                            alert('Acest loc este deja rezervat');
                        }
                        else {
                            $(this).toggleClass(settings.selectingSeatCss);
                        }
                    });
                }
            });
        });
    });

    $('#saveData').click(function() {
        var tag1 = $("#tags1").val();
        var tag2 = $("#tags2").val();
        var date = $('#datepicker').val();
        var n_cop = $("#n_copiii").val();
        var n_adult = $("#n_adult").val();
        var str_place = '';
        var x = 0;
        $("ul li").each(function() {
            if ($(this).hasClass('selectingSeat')) {
                if (x == 0) {
                    str_place += $(this).attr('id');
                } else {
                    str_place += ',' + $(this).attr('id');
                }
                x++;
            }
        });
        if (x == 0) {
            alert("Selectati cel putin un loc pentru a finisa rezervarea");
        } else {
            if (x == 1) {
                $("#message").append("<p id='messbody' >Ati selectat locul " + str_place + " de la  " + $("#tags1").val() + " la  " + $("#tags2").val() +
                        " la ora " + $('#time').val() + ' pe data de ' + date + ". </br><p style='text-indent: 1em;' >Pentru a continua introduceti numele,prenumele dvs. adresa de email si un numar de contact.</p>");
            } else {
                $("#message").append("<p id='messbody' >Ati selectat locurile " + str_place + " de la  " + $("#tags1").val() + " la " + $("#tags2").val() +
                        " la ora " + $('#time').val() + ' pe data de ' + date + ".</br><p style='text-indent: 1em;'>Pentru a continua introduceti numele,prenumele dvs. adresa de email si un numar de contact. </p>");
            }
            $(function() {
                var name = $("#name"),
                        email = $("#email"),
                        prename = $("#prename"),
                        allFields = $([]).add(name).add(email).add(prename),
                        tips = $(".validateTips");
                function updateTips(t) {
                    tips
                            .text(t)
                            .addClass("ui-state-highlight");
                    setTimeout(function() {
                        tips.removeClass("ui-state-highlight", 1500);
                    }, 500);
                }
                function checkLength(o, n, min, max) {
                    if (o.val().length > max || o.val().length < min) {
                        o.addClass("ui-state-error");
                        updateTips("Length of " + n + " must be between " +
                                min + " and " + max + ".");
                        return false;
                    } else {
                        return true;
                    }
                }
                function checkRegexp(o, regexp, n) {
                    if (!(regexp.test(o.val()))) {
                        o.addClass("ui-state-error");
                        updateTips(n);
                        return false;
                    } else {
                        return true;
                    }
                }
                $("#dialog-form").dialog({
                    resizable: false,
                    height: 420,
                    width: 600,
                    modal: true,
                    buttons: {
                        "Confirmati datele": function() {
                            var bValid = true;
                            allFields.removeClass("ui-state-error");
                            bValid = bValid && checkLength(name, "Nume", 3, 16);
                            bValid = bValid && checkLength(prename, "Prenume", 3, 16);
                            bValid = bValid && checkLength(email, "email", 6, 80);
                            bValid = bValid && checkRegexp(name, /^[a-z]([0-9a-z_])+$/i, "Nume incorect .");
                            bValid = bValid && checkRegexp(prename, /^[a-z]([0-9a-z_])+$/i, "Prenume incorect. ");
                            // From jquery.validate.js (by joern), contributed by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
                            bValid = bValid && checkRegexp(email, /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i, "eg. ui@jquery.com");

                            if (bValid) {
                                $.ajax({
                                    type: "POST",
                                    url: "save_data/saveClient",
                                    data: {name: name.val(), prename: prename.val(), email: email.val(), data: date, ora: ora, tags1: tag1, tags2: tag2, idcursa: window.rowId, places: str_place, pret: pret},
                                    dataType: "json",
                                    beforeSend: function() {
                                        var opts = {
                                            lines: 11, // The number of lines to draw
                                            length: 40, // The length of each line
                                            width: 9, // The line thickness
                                            radius: 60, // The radius of the inner circle
                                            corners: 1, // Corner roundness (0..1)
                                            rotate: 0, // The rotation offset
                                            direction: 1, // 1: clockwise, -1: counterclockwise
                                            color: '#000', // #rgb or #rrggbb or array of colors
                                            speed: 0.8, // Rounds per second
                                            trail: 78, // Afterglow percentage
                                            shadow: true, // Whether to render a shadow
                                            hwaccel: false, // Whether to use hardware acceleration
                                            className: 'spinner', // The CSS class to assign to the spinner
                                            zIndex: 2e9, // The z-index (defaults to 2000000000)
                                            top: 'auto', // Top position relative to parent in px
                                            left: 'auto' // Left position relative to parent in px
                                        };
                                        $('#heading').css('opacity', 0.3);
                                        $('#heading').css('z-index', 100);
                                        $('#heading').css('background', 'black');
                                        $("#dialog-form").remove();
                                        var target = document.getElementById('search');
                                        var spinner = new Spinner(opts).spin(target);
					},
                                    success: function(data) {
                                        window.location.replace("admin_page");

                                    }
                                });
                            }
                        },
                        "Inapoi": function() {
                            $(this).dialog("close");
                            $('#message').empty();
                        }
                    }
                });
            });
        }
    });
    $('#mod_caut').click(function() {
        $('#mod_caut').hide();
        $("#search_result").toggle("clip")
        $("table").find("tr:gt(0)").remove();
        $("table").off('click', '.edit_tr');
    });
    $('#back').click(function() {
        $(".client_data").hide();
        $(".place_ser").show();
        $(".container").show();

    })
});


